
const routes = [
  {
    path: '/',
    component: () => import('layouts/Layout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('pages/Auth/Login.vue'),
  },
  {
    path: '/mis-proyectos',
    name: 'mis-proyectos',
    component: () => import('pages/MyProyect.vue'),
  },
  {
    path: '/panel-admin',
    name: 'panel-admin',
    component: () => import('layouts/LayoutAdminGoogle'),
    redirect: '/panel-admin/home',
    children: [
      {
        path: 'home',
        name: 'home',
        component: () => import('pages/Home/HomeAdmin.vue')
      },
      { path: 'administrativo', component: () => import('pages/Moduls/administrativo/index.vue') },

     /* { path: 'administrativo/cliente-proveedor', component: () => import('pages/moduls/administrativo/clienteProveedor/clienteproveedor.vue') },*/

      /** MODULO PROYECTO **/
      /*{
        path: 'administrativo/proyecto',
        meta: { resource: 'proyecto' },
        component: () => import('pages/moduls/administrativo/proyecto/Proyecto.vue')
      },
      {
        path: 'administrativo/proyecto/create',
        meta: { resource: 'proyecto', mode: 'Crear' },
        component: () => import('pages/moduls/administrativo/proyecto/partial/create.vue')
      },
      {
        path: 'administrativo/proyecto/:id',
        meta: { resource: 'proyecto', mode: 'Ver' },
        component: () => import('pages/moduls/administrativo/proyecto/partial/show.vue')
      },
      {
        path: 'administrativo/proyecto/:id/edit',
        meta: { resource: 'proyecto', mode: 'Editar' },
        component: () => import('pages/moduls/administrativo/proyecto/partial/edit.vue')
      },*/
      /** -------------------------------- **/


      //{ path: 'administrativo/usuario', component: () => import('pages/moduls/administrativo/usuario/usuario.vue') },
    ],
    /*beforeEnter: (to, form, next) =>{
      if(myStore.state.login.user.tipousuario == 2){
        next()
      }else{
        if (myStore.state.login.user.totalproyecto >= 2) {
          next({ name: 'mis-proyectos' })
        } else {
          if(myStore.state.login.user.totalproyecto == 0){
            next({ name: 'sin-proyectos' })
          }else{
            next({ name: 'panel-usuario' })
          }
        }
      }
    }*/
  },
  {
    path: '/panel-usuario',
    name: 'panel-usuario',
    component: () => import('layouts/LayoutUserGoogle.vue'),
    redirect: '/panel-usuario/home',
    children: [
      { path: 'home', component: () => import('pages/Home/HomeUser.vue') },
      {
        path: 'proyecto',
        meta: { resource: 'proyecto', mode: 'inicio' },
        component: () => import('pages/Moduls/administrativo/proyecto/index'),
      },
      {
        path: 'proyecto/create',
        meta: { resource: 'proyecto', mode: 'Crear' },
        component: () => import('pages/Moduls/administrativo/proyecto/partial/create.vue')
      },
      {
        path: 'proyecto/:id/show',
        meta: { resource: 'proyecto', mode: 'Ver' },
        component: () => import('pages/Moduls/administrativo/proyecto/partial/show.vue')
      },
      {
        path: 'proyecto/:id/edit',
        meta: { resource: 'proyecto', mode: 'Editar' },
        component: () => import('pages/Moduls/administrativo/proyecto/partial/edit.vue'),
      }
    ]
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
