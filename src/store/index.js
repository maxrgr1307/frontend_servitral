import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'

// import example from './module-example'
import login from './login'
import usuProyecto from './usuProyecto'
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */
let myStore
export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      login,
      usuProyecto
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  //Store.dispatch("login/getUser");

  myStore = Store
  return Store
})
