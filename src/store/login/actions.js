import { api } from '../../boot/axios'
import { SessionStorage } from 'quasar'

export async function loginUser({commit}, credentials){
  //await api.get('/sanctum/csrf-cookie')
  return new Promise((resolve, reject) => {
    api.post('api/auth/login', credentials)
        .then(res => {
          SessionStorage.set('token', res.data.token)
          commit("SET_USER", res.data)
          resolve(res.data)
        }).catch(error => {
          reject(error)
        })
  })
}

export async function getUser({commit}){
  await api.get('api/user')
      .then(res => {
          commit("SET_USER", res.data)
      }).catch(() =>{
          commit("SET_USER", null)
     })
}

export function logout({commit}){
  return new Promise((resolve, reject) => {
    api.post('api/logout').then(res => {
      SessionStorage.set('token', '')
      commit("SET_USER", null)
      resolve(res.data)
    })
  })
}
