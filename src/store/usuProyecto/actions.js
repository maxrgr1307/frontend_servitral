import { api } from '../../boot/axios'

export async function getUsuProyectos({commit}){
  return new Promise((resolve, reject) => {
    api.get('api/getUsuProyectos')
        .then(res => {
          commit("PROYECTO_USUARIO", res.data)
          resolve(res.data)
        }).catch(error => {
          reject(error)
        })
  })
}
export async function resetProyecto({commit}){
  return new Promise((resolve, reject) => {
    api.get('api/resetUsuProyectos')
        .then(res => {
          commit("PROYECTO_USUARIO", res.data)
          resolve(res.data)
        }).catch(error => {
          reject(error)
        })
  })
}
export async function updateUsuProyectos({commit}, uuid){
  return new Promise((resolve, reject) => {
    api.post('api/updateUsuProyectos', {"uuid": uuid })
        .then(res => {
          commit("PROYECTO_USUARIO", res.data)
          resolve(res.data)
        }).catch(error => {
          reject(error)
        })
  })
}
export async function getMenu({commit}, uuid){
  return new Promise((resolve, reject) => {
    api.get('api/getMenu')
        .then(res => {
          commit("USER_MODULOS", res.data)
          resolve(res.data)
        }).catch(error => {
          reject(error)
        })
  })
}
export async function clearProyectoModulo({commit}){
  commit("PROYECTO_USUARIO", [])
  commit("USER_MODULOS", [])
}
