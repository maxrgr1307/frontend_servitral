export function proyectosUsuarios(state){
  return state.proyectosUsuarios.length !=0 ?state.proyectosUsuarios.data:[]
}
export function modulosUsuario(state){
  const modusu = state.modulosUsuario
  return modusu.length ==0 ? []: modusu.data[0].usu_modulos.map(function(x) {
    const submodulos = x.usu_submodulos.map( function(sub){
      const submod = {
        "title": sub.submodulo.nombre,
        "icon":"school",
        "link":sub.submodulo.slug,
        "level":1,
        "children":[]
      }
      return submod
    })
    const essentialLinks =
      {
        "title":x.modulo.nombre,
        "icon":"photo",
        "level":0,
        "children": submodulos
      }
    return essentialLinks;
  });
}

export function allProyectUsuario(state){
  let data = state.proyectosUsuarios.length !=0 ?state.proyectosUsuarios.data:[]
  return data.map((res) => {
    return { uuid: res.uuid, estado: res.estado, nombre: res.proyecto.nombre}
  })
}

export function proyectActive(state){
  let data = state.proyectosUsuarios.length !=0 ?state.proyectosUsuarios.data:[]
  const mmm= { uuid: "0", estado: "0", nombre: "Cargando proyecto..."}
  data.forEach(res => {
    if(res.estado === 2){
      //mmm.splice(res.uuid, res.estado, res.proyecto.nombre)
      mmm.uuid = res.uuid
      mmm.estado = res.estado
      mmm.nombre = res.proyecto.nombre
    }
  });
  return mmm
  /*const isProyectActive = data.filter((res)=> res.estado == 2)
  return isProyectActive.map((res) => {
    return { uuid: res.uuid, estado: res.estado, nombre: res.proyecto.nombre}
  })*/
}
