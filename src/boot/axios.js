import { boot } from 'quasar/wrappers'
import axios from 'axios'

import { SessionStorage } from 'quasar'

let token = SessionStorage.getItem('token')
const api = axios.create({ baseURL: 'http://localhost:8000/' })
api.defaults.headers.common['Authorization'] = 'Bearer ' + token
api.defaults.withCredentials = true

api.interceptors.response.use(
  function(response) {
    //return response;
    //alert('genial')
    console.log(response)
    return response;
  },
  function(error) {
      // handle error
      if (error.response) {
        console.log(error.response)
      }
      /*if(error.response.data.message == 'Unauthenticated.' && window.location.pathname != '/login'){
        console.log(error.response)
        console.log('url', window.location.pathname)
        window.location.href = "/login"
      }*/
  });

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api
  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

// UTILIZAR EN CUALQUIER COMPONENTE ASI : import { api } from 'boot/axios'

export { axios, api }
