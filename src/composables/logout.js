import { useStore } from "vuex";
import { useRouter } from "vue-router";
import { useQuasar } from "quasar";

export default function composableLogout() {

  const store = useStore();
  const router = useRouter();
  const $q = useQuasar();

  const logout = async () => {
    $q.loading.show({ spinnerColor: 'blue-10', backgroundColor: 'bg-fondo', message: 'Cerrando sesión'})
    await store.dispatch("login/logout").then(() => {
      $q.notify({
        type: "warning",
        position: "top-right",
        timeout: 1000,
        message: "La sesión cerro con exito..!",
      });
      router.push({ path: "/" });
      $q.loading.hide()
    });
  };

  return {
    logout,
  }
}


