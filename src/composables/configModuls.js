import { ref } from 'vue'

export default function composableModuls() {

  const cpsModuls = ref([
    { id: 1,  nombre: 'RR.HH', path: 'recursos-humanos', icon:"img_modulos/rr_hh1.svg"},
    { id: 2,  nombre: 'Administrativo', path: 'administrativo', icon:'img_modulos/administrativo2.svg'},
    { id: 3,  nombre: 'Cliente / Proveedor', path: 'cliente-proveedor', icon:'img_modulos/cliente_proveedor.svg'},
    { id: 4,  nombre: 'Operaciones', path: 'operaciones', icon:'img_modulos/operaciones.svg'},
    { id: 5,  nombre: 'Almacen', path: 'almacen', icon:'img_modulos/almacen1.svg'},
    { id: 6,  nombre: 'Micelanea', path: 'miscelanea', icon:'img_modulos/miscelanea.svg'},
    { id: 7,  nombre: 'Maestro', path: 'maestro', icon:'img_modulos/maestro.svg'},
    { id: 8,  nombre: 'Venta', path: 'venta', icon:'img_modulos/venta.svg'},
    { id: 9,  nombre: 'Mantenimiento', path: 'mantenimiento', icon:'img_modulos/mantenimiento.svg'},
    { id: 10, nombre: 'Seguridad', path: 'seguridad', icon:'img_modulos/seguridad.svg'},
    { id: 11, nombre: 'Proyectos', path: '/mis-proyectos', icon:'img_modulos/proyecto.svg'},
  ])

  return {
    cpsModuls
  }
}
