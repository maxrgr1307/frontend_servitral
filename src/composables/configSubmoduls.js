export default function nameTable(valor) {

  const ALL_SUB_MODULOS = {
    'proyectos': {
      url: "api/proyectos",
      orderables: [
          { cell: 'A1', label: "ID", column: "id", hidden: false, width: '70px'},
          { cell: 'B1', label: "Nombre", column: "nombre", hidden: false, width: ''},
          { cell: 'C1', label: "Departamento", column: "departamento_id", hidden: true, width: ''},
          { cell: 'D1', label: "Provincia", column: "provincia_id", hidden: true, width: ''},
          { cell: 'E1', label: "Distrito", column: "distrito_id", hidden: true, width: ''},
          { cell: 'F1', label: "Proveedor", column: "proveedor_id", hidden: false, width: ''},
          { cell: 'G1', label: "Fecha inicial", column: "fchainicial", hidden: false, width: '140px'},
          { cell: 'H1', label: "Fecha final", column: "fchafinal", hidden: false, width: '140px'},
          { cell: 'I1', label: "Observaciones", column: "observacion", hidden: true, width: ''},
          { cell: 'J1', label: "Alm_sigla", column: "alm_sigla", hidden: true, width: ''},
          { cell: 'K1', label: "Alm_serie", column: "alm_serie", hidden: true, width: ''},
          { cell: 'M1', label: "Com_sigla", column: "com_sigla", hidden: true, width: ''},
          { cell: 'N1', label: "Com_serie", column: "com_serie", hidden: true, width: ''},
          { cell: 'Ñ1', label: "Caj_sigla", column: "caj_sigla", hidden: true, width: ''},
          { cell: 'O1', label: "Caj_serie", column: "caj_serie", hidden: true, width: ''},
          { cell: 'P1', label: "Estado", column: "estado", hidden: false, width: '120px'},
          { cell: 'Q1', label: "Fecha Creacion", column: "created_at", hidden: true, width: '158px'},
          { cell: 'R1', label: "Opciones", column: "Opciones", hidden: true, width: '120px'},
      ],
      filterGroups: [
          {
            name: "Proyectos",
            filters: [
              { title: "Id", name: "id", type: "numeric" },
              { title: "Nombre", name: "nombre", type: "string" },
              { title: "Departamento", name: "departamento", type: "string" },
              { title: "Provincia", name: "provincia", type: "string" },
              { title: "Distrito", name: "distrito", type: "string" },
              { title: "Idproveedor", name: "idproveedor", type: "string" },
              { title: "Fecha inicial", name: "fchainicial", type: "numeric" },
              { title: "Fecha final", name: "fchafinal", type: "numeric" },
              { title: "Observaciones", name: "observaciones", type: "string" },
              { title: "Alm_sigla", name: "alm_sigla", type: "numeric" },
              { title: "Alm_serie", name: "alm_serie", type: "numeric" },
              { title: "Com_sigla", name: "com_sigla", type: "numeric" },
              { title: "Com_serie", name: "com_serie", type: "numeric" },
              { title: "Caj_sigla", name: "caj_sigla", type: "numeric" },
              { title: "Caj_serie", name: "caj_serie", type: "numeric" },
              { title: "Estado", name: "estado", type: "static_lookup" },
              { title: "Fecha Creacion", name: "created_at", type: "datetime" },
            ],
          },
      ]
    }
  }
  const table = ALL_SUB_MODULOS[valor] || ''

  return {
    table
  }
}


